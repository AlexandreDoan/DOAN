## Mission 0 : Découverte d'une architecture réseau
Doan Alexandre 
Date de création : 16/10/2020 
Date de modification : 05/11/2020
    
#### I - Quel est le nom et le rôle des équipements réseaux suivants ?
- Pare feu (interne ou externe)
- Routeur Internet Fonction NAT activée
- Commutateur WIFI

#### II- Que signifient les informations suivantes ?

•	Serveur Web : est soit un logiciel de service de ressources web (serveur HTTP), soit un serveur informatique (ordinateur) qui répond à des requêtes du World Wide Web sur un réseau public (Internet) ou privé (intranet), en utilisant principalement le protocole HTTP.

•	Serveur DNS : Le serveur DNS (Domain Name System, ou Système de noms de domaine en français) est un service dont la principale fonction est de traduire un nom de domaine en adresse IP.

•	Serveur DHCP : L'adresse IP doit être unique sur un réseau donc le serveur DHCP (ou service DHCP) va gérer les adresses et n'attribuer que des adresses non utilisées à tout nouvel hôte qui en fait la demande.

•	Serveur Proxy : Un serveur proxy joue le rôle de passerelle entre Internet et vous. C’est un serveur intermédiaire qui sépare les utilisateurs, des sites Web sur lesquels ils naviguent. Les serveurs proxy assurent différents niveaux de fonctionnalité, de sécurité et de confidentialité, selon votre type d’utilisation, vos besoins ou la politique de votre entreprise.

•	Serveur SMTP et POP : SMTP et POP3 sont deux des aspects techniques les plus importants de l'ensemble du processus d'envoi d'un courriel. En un mot, le SMTP (acronyme de Simple Mail Transfer Protocol) s'occupe de la livraison, tandis que le protocole POP (acronyme de Post Office Protocol) s'occupe du téléchargement sur votre ordinateur.

•	Routeur fonction NAT : La fonction NAT dans un routeur de service intégré (ISR) traduit une adresse IP source interne en adresse IP globale. Ce procédé est très largement utilisé par les box internet (ou modem routeur) des fournisseurs d'accès pour cacher les ordinateurs domestiques derrière une seule identification publique.

•	DMZ : En informatique, une zone démilitarisée, ou DMZ (en anglais, demilitarized zone) est un sous-réseau séparé du réseau local et isolé de celui-ci et d'Internet (ou d'un autre réseau) par un pare-feu.

•	172.30.32.0 / 19 : Adresse IP iPV4 

#### III- Identifiez les différentes catégories d’ordinateurs présents sur le réseau ? 
    
Postes fixes, serveurs et des tablettes (Routeurs et commutateur sont du matériel réseau)

#### IV- Identifiez les mots toujours inexpliqués et cités dans la mission.

-RPV : Réseau Privé Virtuel : Virtual Private Network ou VPN en anglais. ... L'idée de base du RPV est donc de faire transiter les données sur un réseau public (téléphonique, relais de trame et surtout Internet), qui implique des coûts de communications inférieurs.

-Serveur de base de données : Un serveur de base de données sert à stocker, à extraire et à gérer les données dans une base de données. Il permet également de gérer la mise à jour des données. Il donne un accès simultané à cette base à plusieurs serveurs Web et utilisateurs.

-Serveur d'application : Un serveur d'applications est un logiciel d'infrastructure offrant un contexte d'exécution pour des composants applicatifs. Le terme est apparu dans le domaine des applications web.

-Serveur de fichiers : Un serveur de fichiers permet de partager des données à travers un réseau. Le terme désigne souvent l'ordinateur hébergeant le service applicatif. Il possède généralement une grande quantité d'espace disque où sont déposés des fichiers.