## Mission 1 : Découverte de la simulation réseau avec Filius
Doan Alexandre
Date de création : 01/11/2020
Date de modifications : 05/11/2020

### Etape 1 :
    
#### Quel est le masque de sous-réseau de chaque machine ?

Le masque de sous-réseau de chaque machine est 255.255.0.0.

#### A quel sous-réseau IP appartient chaque machine ? Justifier.
Le sous-réseau IP est 11000000 10101000 00000000 00000000 donc 192.168.0.0 car :
        
IP: 192 168 10 10 = 11000000 10101000 1010 1010
        
MSK: 255 255 0 0 = 11111111 11111111 00000000 00000000
        
SSR: 196 168 10 10 = 11000000 10101000 00000000 00000000

#### Peuvent-elles potentiellement communiquer entre elles ? Justifier.

Non, elle ne peuvent pas communiquer entre elles, puisqu'elle n'ont pas la même adresse IP.

#### Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles ? Justifier.

Dans les conditions physiques présentées, elles ne peuvent pas communiquer entre elles car il manque des éléments d'interconnexion tel que le Switch.

#### Quelle commande sur le terminal Filius permet de voir la configuration IP d’une STA ?

La commande sur le terminal Filius permettant de voir la configuration IP est #ipconfig.

#### Quelle commande permet de tester que les messages IP sont bien transmis entre deux machines ?

La commande qui permet de tester que les messages IP sont bien transmis entre deux machines est #ping+l'adresse de la machine.

### Etape 2 : 

#### Quel média de communication a été ajouté ? Expliquer brièvement son fonctionnement.

Le média de communication qui a été ajouté est le Switch (le switch est un boîtier doré de quatre à plusieurs centaines de ports Ethernet, et qui sert à relier en réseau différents éléments du système informatique).

#### Quel autre média aurait pu le remplacer (inexistant sur Filius) ? En quoi est-il différent du premier ?

L'autre média qui aurait pu remplacer le switch est le concentrateur. Le concentrateur fonctionne de façons binaire, il apparaît au niveau 1 du modèle OSI.

#### Est-ce que toutes les machines peuvent maintenant communiquer entre elles ? Justifier.

Non, toutes les machines ne peuvent pas communiquer entre elles car M2 et M3 on la même adresse IP.

#### Si nécessaire, corriger les configurations pour permettre à toutes les STAs de communiquer entre elles. Quelles sont les modifications apportées ?

On modifie l'adresse IP de (M3) pour qu'elle puisse communiquer avec les autres (exemple:M3 192.168.13.0).

#### Si M1 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse logique peut-elle utiliser ?

Si M1 veut envoyer un message à toutes les machines de son sous-réseau, l'adresse logiquequ'elle peut utiliser est l'adresse logique qu'elle peut utiliser est l'adresse de broadcast (192.168.255.255)

### Etape 3 : 

#### A quel réseau appartient la 4e machine (M4) ? Est-ce le même que celui des Machines M1, M2 et M3 ? Justifier.

Le réseau qu'appartient la 4e machine est 192.169.0.10 Ce n'est pas le même que celui des machines M1 M2 M3 car elles appartiennent au réseau 192.168.0.10

#### M4 peut-elle communiquer avec M1, M2 ou M3 ? Justifier.

M4 ne peut pas communiquer avec M1 M2 ou M3  car elles n'ont pas la même adresse IP réseau.

#### Si nécessaire, reconfigurer M2 pour lui permettre d’échanger des messages avec M4. Quels sont les changements nécessaires ?
    
Pour permettre à M2 d'échanger des messages avec M4, il faut changer son adresse IP 192.168.0.10par exemple en 192.168.0.10, par exemple en 192.168.10.10 pour qu'elle puisse communiquere entre elle.

#### Est-ce que M1 et M2 peuvent toujours communiquer ? Justifier.
 
M1 ET M2 ne peuvent pas communiquuer entre elles car M1 n'a pas le même réseau que M2.
