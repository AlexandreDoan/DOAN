## Mission 2 : Simulation Filius d'échanges entre deux sous-réseaux logiques
Doan Alexandre

Date de création : 01/11/2020

Date de modification : 05/11/2020 

### Etape 1 :

#### Quel est le CIDR de chaque machine ? Justifier.
Le masque de la machine(M1) est 255.255.0.0 donc le CIDR est /16. 

Le masque de la machine(M2) est 255.0.0.0 donc le CIDR est /8.

Le masque de la machine(M3) est 255.0.0.0 donc le CIDR est /8.

Le masque de la machine(M4) est 255.255.0.0 donc le CIDR est /16.

#### A quel sous-réseau IP appartient chacune de ces 4 machines ? Justifier.
Le sous-réseau IP de la machine(M1) est 192.168.0.0/16.

Le sous-réseau IP de la machine(M2) est 10.0.0.0/8.

Le sous-réseau IP de la machine(M3) est 10.0.0.0/8.

Le sous-réseau IP de la machine(M4) est 192.168.0.0/16.

#### Combien de machines maximums peuvent accueillir chacun des sous-réseaux identifiés ?

Dans les sous-réseaux identifiés il peut accueillir 2 machines maximums.

#### Quelles machines peuvent potentiellement communiquer entre elles ? Justifier.

La machine M1 peut communiquer avec la machine M4 car ils ont le même sous réseau.

La machine M2 peut communiquer avec la machine M3 car ils ont le même sous réseau.

#### Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles ? Justifier.

Dans les conditions actuel les machines ne peuvent pas communiquer entre elles car il n'y a pas de routeur. 

#### Quelle est la commande complète qui permet à M1 de tester la liaison avec M2

La commande qui permet de tester les liaison est "ipconfi".

### Etape 2 : 

#### Compléter la configuration des machines M5 et M6 pour permettre à M5 d'échanger avec M1 et permettre à M6 d'échanger avec M2.

#### Quelles sont les adresses IP possibles pour M5 et M6 ?

#### Combien de machines peut-on encore ajouter dans chaque sous-réseau ?

#### Si M6 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse IP peut-elle utiliser ?

#### Quel média d'interconnexion est nécessaire pour permettre à toutes les machines d'échanger des messages ?


### Etape 3 :

#### Expliquer brièvement le fonctionnement d’un routeur.
    
#### Compléter la configuration physique pour permettre aux différentes machines des différents réseaux logiques de communiquer.

##### Combien d’interfaces réseaux sont nécessaires ?
##### Quelle adresse IP aura chaque interface ?

#### Est-ce que toutes les machines peuvent maintenant communiquer entre elles ? Justifier.

#### Quelle nouvelle configuration est nécessaire pour permettre aux machines de communiquer avec des machines appartenant à d’autres réseaux (M5 avec M6 par exemple)?