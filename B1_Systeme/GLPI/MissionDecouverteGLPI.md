## Mission  : Découverte  GLPI
## Doan Alexandre
## Cours Bloc1-Système(BTS - SIO) - 11/03/2021


****

1.	Que signifie le sigle GLPI ?

Un GLPI (Gestionnaire Libre de Parc Informatique) est un logiciel libre de gestion des services informatiques (ITSM) et de gestion des services d'assistance (issue tracking system et ServiceDesk). ... GLPI est une application web qui aide les entreprises à gérer leur système d'information.

Source : https://fr.wikipedia.org/wiki/Gestionnaire_Libre_de_Parc_Informatique

2.	Qu’est-ce que GLPI ? 

C'est un Gestionnaire Libre de Parc Informatiquec cette solution libre est éditée en PHP et distribuée sous licence GPL. En tant que technologie libre, toute personne peut exécuter, modifier ou développer le code qui est libre.

Source : https://fr.wikipedia.org/wiki/Gestionnaire_Libre_de_Parc_Informatique

3.	Citez les principaux  modules qui composent l’application GLPI.

Il existe 6 modules qui composent l'application GLPI:

   - Le module Parc 
   - Le module Assistance 
   - Le module Gestion 
   - Le module Outils 
   - Le module Administration 
   - Le module Configuration 

Source : https://glpi-project.org/DOC/FR/glpi/navigate.html

4.	Décrivez les modules cités dans la question précédente (question n°3).

   - Le module Parc permet d'accéder aux différents matériels inventoriés.
   - Le module Assistance permet de créer, suivre les tickets, et voir les statistiques.
   - Le module Gestion permet de gérer les contacts, fournisseurs, budgets, contrats et documents.
   - Le module Outils permet de gérer des notes, la base de connaissance, les réservations, les flux RSS et visualiser les rapports.
   - Le module Administration permet d'administrer les utilisateurs, groupes, entités, profils, règles et dictionnaires. Il permet aussi la maintenance de l'application (sauvegarde et restauration de base, vérifier si une nouvelle version est disponible).
   - Le module Configuration permet d'accéder aux options de configuration générale de GLPI : notifications, collecteurs, tâches automatiques, authentification, plugins, liens externes.

Source : https://glpi-project.org/DOC/FR/glpi/navigate.html

5.	Quels sont, d’après vous, les avantages que peut présenter cette application ? 

Les avantages que peut présenter cette application sont : 

- Un inventaire automatisé avec remontée des informations matérielles et logicielles par un agent 

- La réduction des coûts 

- L'optimisation des ressources 

- La gestion rigoureuse des licences 

- Une démarche qualité 

- La satisfaction utilisateur 

- La sécurité

Source : https://www.memoireonline.com/08/18/10233/m_Administration-et-securisation-dune-parc-informatique-avec-GLPI-OCS-NG-et-Windows-server-2008-r8.html

6.	Trouvez des solutions semblables à GLPI, présentes sur le marché ? 

- OCS Inventory
- FusionInventory
- XAMPP

Source : https://www.google.com/search?client=firefox-b-d&sa=X&sxsrf=ALeKk00ifa3iAp85UW7TB0M_-EcbNsQjkg:1615456747303&q=OCS+Inventory&stick=H4sIAAAAAAAAAONgFuLSz9U3SDJLiy83VwKzjYrSiyzTtISzk630k_Nzc_PzrIozU1LLEyuLF7Hy-jsHK3jmlaXmleQXVe5gZZzAxggAAigLfkUAAAA&ved=2ahUKEwjynMqs_afvAhUPExoKHWPlApEQ-BYwIHoECAwQOA&biw=1920&bih=938

****
