## Activité Wireshark et application de chat **Gato**
## Doan Alexandre
## Bloc1 - Système (BTS - SIO) - 08/01/2021

### Etape 1 - Installer Wireshark 

1.Wireshark est un analyseur de paquets libre et gratuit. Il est utilisé dans le dépannage et l’analyse de réseaux informatiques, le développement de protocoles, l’éducation et la rétro-ingénierie. 
Il servira a faire une capture.

![Wireshark](img/Wireshark.PNG)  

### Etape 2 - Analyser le trafic généré par votre serveur http NodeJS

2.
3.
4.
5.

6. Filtrer les paquets HTTP échangés :

 	- Comment différencier les requêtes des réponses ? Pour différencier les requêtes des réponse nous avons besoin de l'adresse source et l'adresse de destination ils vont permettre de savoir d'où viens l'information et qui sera le destinataire.

	- Combien de paires (requête et réponse) ont été échangées ? Il y a 14 paires de requêtes et réponse qui ont était échangés sous le protocole HTTP.

	- Voit-on les messages écrits via Gato dans le contenu des trames ? Non car les messages sont sécurisé et crypté.

7. Filtrer les paquets Websocket échangés : 

	- Comment différencier les requêtes des réponses ? Pour différencier les requêtes des réponse nous avons besoin de l'adresse source et l'adresse de destination comme pour les paquets HTTP échangés.

	- Combien de paires (requête et réponse) ont été échangées ? Il y a 13 paires de requêtes qui ont était échangés sous le protocole Websocket.

	- Voit-on les messages écrits via Gato dans le contenu des trames ? Oui car grâce au protocoles Websocket les messages ne sont pas sécurisé et crypté.
