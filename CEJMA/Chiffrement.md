#### Doan Alexandre
#### BTS 2 / ECJMA 
#### Date de création : 09/03/2022

## Chiffrement 

# Première partie : Cryptographie symétrique

### Quels sont les caractéristiques de l’algorithme AES (année, avantages, inconvénients, usages recommandé,comparaison avec 3DES) ?

 L'AES (Advanced Encryption Standard) est, comme son nom l'indique, un standard de cryptage symétrique destiné à remplacer le DES (Data Encryption Standard) qui est devenu trop faible au regard des attaques actuelles.
- Avantage : Rapidité des opérations de chiffrement et déchiffrement 
- Inconvénient : Transmission de la clé secrète de chiffrement (la procédure doit être sécurisée) 
- Année : 2 janvier 1997
- L'AES est un standard, donc libre d'utilisation, sans restriction d'usage ni brevet.
- C'est un algorithme de type symétrique (comme le DES) 
- C'est un algorithme de chiffrement par blocs (comme le DES)


Nous allons comparer ici l'AES au 3DES qui est son concurrent le plus direct (le DES n'étant pratiquement plus utilisé dans sa forme simple). Le 3DES est, comme son nom l'indique, l'enchainement de 3 DES simples dans l'ordre DES / DES-1 / DES. Il est évident à prime abord que chaque opération utilise une clé distincte, car sans cela les 2 premières s'annuleraient (DES / DES-1). Mais en pratique, on n'utilise que 2 clés différentes (que l'on alterne) car l'utilisation d'une troisième clé ne rajoute aucune sécurité.

En effet, l'attaque la plus courante contre le triple DES consiste à créer des dictionnaires multiples de facon à scinder le schéma en 2 parties et diminuer ainsi d'autant le nombre de possibilités à tester. En pratique, on séparera les 2 premières opérations DES de la 3ième et dernière.




### Quels sont les critères à prendre en compte pour déterminer le niveau de sécurité dont une organisation a besoin ?
- Sécurité ou l'effort requis pour une éventuelle cryptanalyse.
- Facilité de calcul : cela entraine une grande rapidité de traitement
- Besoins en ressources et mémoire très faibles
- Flexibilité d'implémentation: cela inclut une grande variété de plateformes et d'applications ainsi que des tailles de clés et de blocs supplémentaires (c.f. ci-dessus).
- Hardware et software : il est possible d'implémenter l'AES aussi bien sous forme logicielle que matérielle (câblé)
- Simplicité : le design de l'AES est relativement simple



### Quelles sont les recommandations de l’ANSSI sur la longueur des clés symétriques ? Présentez un exemple d’utilisation de l’algorithme AES.

La taille recommandée pour les systèmes symétriques est de 128 bits.

Exemple : WhatsApp utilise l'algorithme AES.

# Deuxième partie : Cryptographie asymétrique (à clé publique)

### Quels sont noms des inventeurs de l’algorithme de chiffrement RSA et à quelle date ont-ils publiés le principe de cet algorithme ?

Cet algorithme a été décrit en 1977 par Ronald Rivest, Adi Shamir et Leonard Adleman. RSA a été breveté par le Massachusetts Institute of Technology (MIT) en 1983 aux États-Unis. Le brevet a expiré le 21 septembre 2000.

### Quels sont les deux usages de ce chiffrement asymétrique ?

Crypter et signer.

### Quelle est la longueur des clés que recommande l’ANSSI en France pour la période actuelle ?

La taille recommandée pour les systèmes symétriques est de 128 bits.

### A quel usage est initialement destiné l’algorithme de chiffrement DSA ?

Le Digital Signature Algorithm, plus connu sous le sigle DSA, est un algorithme de signature numérique standardisé par le NIST aux États-Unis.

### Indiquez les différentes étapes nécessaires à la création de ce tunnel de communication sécurisé SSH.
- Le serveur envoie sa clé publique au client.


- Le client génère une clé secrète et l'envoie au serveur, en cryptant l'échange avec la clé publique du serveur (cryptographique asymétrique). Le serveur décrypte la clé secrète en utilisant sa clé privée, ce qui prouve qu'il est bien le vrai serveur.


- Pour le prouver au client, il crypte un message standard avec la clé secrète et l'envoie au client. Si le client retrouve le message standard en utilisant la clé secrète, il a la preuve que le serveur est bien le vrai serveur.


- Une fois la clé secrète échangée, le client et le serveur peuvent alors établir un canal sécurisé grâce à la clé secrète commune (cryptographie symétrique).


- Une fois que le canal sécurisé est en place, le client va pouvoir envoyer au serveur le login et le mot de passe de l'utilisateur pour vérification. La canal sécurisé reste en place jusqu'à ce que l'utilisateur se déloggue.


### Comment peut-on s’authentifier par clé pour le service SSH ?

Au lieu de s'authentifier par mot de passe, les utilisateurs peuvent s'authentifier grâce à la cryptographie asymétrique et son couple de clés privée/publique, comme le fait le serveur SSH auprès du client SSH.

# Troisième partie : Signature numérique

### Quelles autres fonctions de hachage est-il conseillé d’utiliser à la place de MD5 ?

### Identifier deux usages des fonctions de hachage.

### Présentez une offre de gestion de la signature électronique :

- Objectif de la réglementation européenne eIDAS
- Exigences de fiabilité de la signature électronique
- Les différents niveaux de la signature électronique du règlement eIDAS
- Les enjeux
- Quelques usages de la signature électronique
