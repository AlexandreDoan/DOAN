#### Doan Alexandre
#### BTS 2 / ECJMA 
#### Date de création : 10/11/2021

## Les contrats informatiques


Ressources
- https://www.captaincontrat.com/articles-droit-commercial/contrat-de-prestation-informatique 
- https://freshservice.com/fr/it-asset-management-software/ 

A l’aide des ressources ci-dessus, répondre aux questions suivantes : 

**1) Qu’est-ce qu’un contrat de prestations de services informatiques ?**

Un contrat de prestations de services informatiques est un contrat qui a pour but de louer une prestation de services informatiques. Il s’agit d’une convention ou contrat d’entreprise entre 2 entreprises pour effectuer un travail relevant du milieu informatique.

**2) Qu’est-ce que le contrat d’out-sourcing ?**

Le contrat d’out-sourcing s’agit d’une externalisation d’un service d’une l’entreprise qui est délégué à une autre entreprise spécialisé dans le sujet, par exemple son réseau informatique.
L’entreprise qui externalise un service peut ensuite se concentrer sur ça fonction première.
 
**3) Identifier les mentions obligatoires (ou recommandées) à indiquer dans un contrat de prestation informatique ?**

Les mentions obligatoires ou recommandées à indiquer dans un contrat de prestation informatique sont : 
- l’objet du contrat
- les prix des prestations de services
- les modalités d’exécution des prestations
- la durée du contrat
- les obligations des parties
- les modalités de rupture
- les modalités de résiliation et de sanction
- les cas de force majeure
- la clause liée au litige (ex : clause d’arbitrage)

**4) Quelle différence existe-t-il entre un contrat d’outsourcing et un contrat ASP (Application Service Provider) ?**

Un contrat ASP ne permet qu’un droit d’accès et d’utilisation de logiciels hébergés par un prestataire, c’est accès est à distance à un système informatique.
Au contraire l’out-sourcing permet de confier la totalité d’une fonction ou d’un service à un prestataire externe, c’est un service complet car le prestataire fournit ses services compte tenu du niveau que l’entreprise à choisit.

**5) Qu’est-ce que la clause de réversibilité ?**

La clause de réversibilité s’agit d’une procédure permettant dans le cas d’un contrat d’out-sourcing, qui permet de reprendre les éléments qui font l’objet de prestation (faillite du prestataire, inexécution du contrat ou fin du contrat).

**6) Quelles sont les différences entre un contrat de sous-traitance et un contrat de prestations de services ?**

Un contrat de prestations consiste à établir un contrat entre entreprise pour effectuer un travail que l’entreprise (cliente) ne peut réaliser.
Alors qu’un contrat de sous traitance consiste en une aide, l’entreprise demandeur reste la responsable.

**7) Identifier les différents types de contrats informatiques**

Il existe plusieurs types de contrats informatiques :
- Le contrat de maintenance informatique
- Le contrat de développement de logiciel informatique spécifique
- Le contrat d'intégration de logiciels informatique
- Le contrat ASP (Application Service Provider)
- Le contrat SAAS (Software AS A Service)

**8) Quelles sont les 3 obligations (devoirs) du prestataire informatique ?**

Les 3 obligations du prestataire informatique sont :
- Le devoir de renseignement
- Le devoir de mise en garde
- Le devoir de conseil

**9) Quels sont les deux types de rémunération du prestataire informatique ?**

Il y a deux types de rémunération du prestataire informatique possible pour un contrat: 
- Rémunération en fonction des objectifs atteints 
- Rémunération en fonction des ressources humaines et matériels mis à disposition

**10) L’externalisation relève-t-elle d’un contrat de sous-traitance ou de prestation de service ?**

 L’externalisation relève que c’est un contrat de sous-traitance.
