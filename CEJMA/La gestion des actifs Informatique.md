#### Doan Alexandre
#### BTS 2 / ECJMA 
#### Date de création : 06/10/2021

## La gestion des actifs Informatique
    
## 1 – Qu’est-ce que la gestion des actifs informatiques ?

**Définir la gestion des actifs informatiques**

La gestion des actifs informatiques consiste à gérer efficacement un équipement informatique tout au long de son cycle de vie ou de la période d'acquisition pour en maximiser la valeur.

**Quelle est la composition de l’actif informatique ?**

Un actif informatique comprend le matériel, les systèmes logiciels ou les informations précieuses pour une organisation. Parmi ses principaux actifs, notre service informatique compte des ordinateurs et des licences logicielles qui nous aident à développer, à vendre et à prendre en charge nos logiciels et les serveurs sur lesquels nous les hébergeons.

**Quels sont les avantages de la gestion des actifs informatiques ?**



## 2 – Les différents impacts de la gestion des actifs informatiques

La gestion du patrimoine informatique implique donc de s'intéresser au contenu des contrats informatiques 
conclus par l'organisation avec ses différents partenaires et notamment les clauses relatives à : 
- la garantie, 
- la durée du contrat, 
- la limitation de la responsabilité du prestataire, 
- les modalités de rupture du contrat en cas de changement de prestataire ou de ré-internalisation de 
- l'activité informatique,
- etc..

La gestion des actifs logiciels (Software Asset Management) dont l'utilisation est encadrée par des contrats 
de licences logicielles nécessite de s’intéresser aux points suivants : 
- cerner les droits accordés aux utilisateurs,
- les divers modèles de tarification,
- les effets de la clause d'audit de plus en plus présente dans les contrats de licences. 

La gestion des sauvegardes est également une activité essentielle dans la gestion du patrimoine informatique 
et elle s'inscrit dans un cadre juridique pour prendre en compte les obligations en matière : 
- d'archivage de données (respecter les durées minimales de conservation des données),
- d'obligation d'assurer l'intégrité, la disponibilité, la sécurité, la confidentialité et la traçabilité de ces données afin de produire une preuve recevable en cas de contentieux.

La gestion du patrimoine informatique suppose que l'ensemble des utilisateurs respectent les règles 
d'utilisation des ressources numériques fixées généralement dans une charte informatique dont la valeur 
juridique dépend des conditions de son élaboration : 
- le rôle de ce document interne à l'organisation ;
- la portée des règles qui y figurent ;
- la compréhension que le non-respect de ces règles engage la responsabilité civile et/ou pénale
du salarié

## 3 – L’audit de licence logiciel

**Quels sont les enjeux des audits réalisés pour les éditeurs de logiciel ?** 

Les enjeux financiers de ces audits sont importants : les éditeurs obtiennent des revenus supplémentaires non négligeables au titre des redevances de régularisation, ce qui implique pour les sociétés utilisatrices de faire face à des coûts imprévus, dont les montants sont parfois très élevés.

**Les entreprises peuvent-elles refuser un audit d'un éditeur de logiciel ?** 

Face à de telles pratiques, les sociétés utilisatrices opposent régulièrement aux éditeurs leur bonne foi en invoquant : la croissance externe de l’entreprise (fusion / acquisition), l’augmentation du nombre de salariés, la démultiplication des solutions logicielles en interne, la complexité des termes de la licence ou encore les modifications successives du contrat faites par l’éditeur.

**De quelles manières peut être réalisé cet audit de l'éditeur d'un logiciel ?**

La simple demande d’informations, l’installation d’outils de comptabilisation des licences, la réalisation de vérifications « sur site » par un auditeur indépendant, ou encore (bien que moins fréquent) un audit sur autorisation du juge et en présence d’experts techniques et d’huissiers de justice.

**Quelles sont les recommandations de l'auteure aux organisations pour permettre une meilleure gestion de ses actifs logiciels (Software Asset Management - SAM)** 

- Encadrer le contrôle par l’éditeur des conditions d’utilisation des logiciels en intégrant des clauses d’audit exemptes de toute ambiguïté (détaillant les modalités de la notification par l’éditeur, la fréquence de l’audit, le déroulé de l’audit, son périmètre, le recours à un tiers auditeur, les couts associés, le mode de fixation des éventuelles redevances complémentaires, etc.);
- Négocier l’insertion de clause d’ajustement, vous permettant sur la base d’une déclaration sur l’honneur semestrielle ou annuelle de déterminer le nombre exact de licences utilisées et d’ajuster le contrat et les redevances versées en conséquence ;
- Négocier une gestion flexible des licences, notamment en négociant les réallocations intra-groupe sans surcoût, permettant la compensation entres filiales des licences excédentaires avec les licences manquantes.

Si le contrat de licence est clair et qu’il a bien été négocié, un suivi de la conformité en interne permettra de mieux appréhender les opérations d’audit.

- Suivre régulièrement les évolutions de votre parc informatique, en réalisant des inventaires des matériels, des logiciels installés et utilisés sur les serveurs et postes de travail, et des licences, ainsi que la relation avec la facturation ;
- Procéder à une sensibilisation de votre personnel à l’égard des risques qu’implique une utilisation non-autorisée des logiciels ;
- Être plus proactif en réalisant des audits à blanc ou des analyses annuelles des risques pour certains éditeurs « sensibles ».
