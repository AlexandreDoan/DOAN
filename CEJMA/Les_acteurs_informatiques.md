#### Doan Alexandre
#### BTS 2 / ECJMA (BLOC 1 et 2)
#### Date de création : 08/09/2021
#### Date de modification : 06/10/2021

## Mission : Typologie des acteurs de l'industrie informatique 

## 1- Identifier les acteurs de l'industrie Informatique

- **Les constructeurs** : Un constructeur informatique est une entreprise qui conçoit, développe, produit, commercialise et maintient des plates-formes informatiques, ainsi que les systèmes d'exploitation qui permettent de les faire fonctionner.

- **les éditeurs de logiciels** : Un éditeur de logiciel est une entreprise qui assure la conception, le développement et la commercialisation de produits logiciels. Elle peut confier la mise en œuvre, l'intégration et la personnalisation à des entreprises de services du numérique.

- **les distributeurs et vendeur de solutions** : Il s'agit de grossiste possèdant du matériel informatique, qui se démarche auprès des entreprseis dont le but est de vendre des solutions (logiciels, matériels ou les deux) en quantité.

- **les intégrateurs** : Un intégrateur d'infrastructure informatique installe les outils (matériels et logiciels) provenant d'éditeurs et de constructeurs dans un système d'information. Il s'agit d'un informaticien généraliste qui précède et collabore avec l'administrateur réseau.

- **les infogérants** :  L'infogérant est la personne qui s'occupe de l'infogérance. Et L'infogérance définit le fait d'externaliser certaines prestations de gestion ou d'exploitation d'un système informatique, à un prestataire informatique tiers. ... La mise en place de cette forme d'externalisation impose la rédaction d'un contrat entre les deux parties.

- **les hébergeurs** : Un hébergeur (communément appelé hébergeur Web ou hébergeur Internet) désigne dans le langage informatique l'entité qui propose comme service l'hébergement d'un site Web.

- **les FAI** : C'est une entreprise qui propose comme service, la fourniture d'accès internet dont les entreprises on un besoin vital désormais. Les hébergeurs peuvent variés selon si vous êtes un particulier ou une société.

- **les ESN (SSII)** : L'ancien acronyme SSII, pour Société de Services et d'Ingénierie en Informatique, était utilisé avant 2013 pour définir une société spécialisée dans le domaine des nouvelles technologies et de l'informatique. Depuis cette date, on emploie désormais l'acronyme ESN, pour Entreprise de Services du Numérique.

- **les services cloud** : Le Cloud Computing est un terme général employé pour désigner la livraison de ressources et de services à la demande par internet. Il désigne le stockage et l'accès aux données par l'intermédiaire d'internet plutôt que via le disque dur d'un ordinateur.

## 2 – L’infogérance
**Définir l’infogérance**

L'infogérance consiste à déléguer la maintenance, la gestion, l'optimisation et la sécurité de votre système d'information à un prestataire informatique. C'est à dire que vous confiez votre système d'information (ordinateur, données ..) à un spécialiste.

**Qu’est-ce que la disponibilité ?**

Le terme «disponibilité» désigne la probabilité qu’un service soit en état de fonctionnement à un instant donné. On sait que plus le taux de disponibilité est élevé moins il y a d’interruption de service et donc plus l’équipement est disponible.

**L’infogérance fait l’objet d’un contrat (forfait/régie/annuel). Quelle est la différence ?**
- Dans un contrat au forfait, le prix unitaire est décidé en échange de la livraison d'un bien ou service défini dans le contrat, dont la conformité à la commande est théoriquement facile à mesurer et à vérifier.
- Au contraire, lorsque les prestations sont réglées en régie, le prix finalement payé par le client prend en compte les débours réels du prestataire (majorés d'un coefficient de frais généraux et de sa marge) et le coût final est donc fonction des surcoûts générés par les aléas que le prestataire peut rencontrer dans son travail

**Quels sont les objectifs du contrat d’infogérance ?**     
Les objectifs sont : 

- Limiter les probabilités de panne au travers de mesures préventives et proactives

- Rétablir le service défaillant dans les meilleurs délais 

- Alignement du système d'information aux besoins métiers 

**Repérer le rôle de l’infogérance au sein des PME&ETI**
L’infogérance offre en effet de nombreux bénéfices aux PME et ETI tels que le contrôle
des coûts, l’adaptabilité et la réactivité en cas d’incidents.

**Identifier les avantages de l’infogérance**

- **DÉLÉGUER À UN EXPERT :** Se charge de vous accompagner et de proposer les solutions adaptées à vos besoins.

- **BÉNÉFICIER D’UNE ASSISTANCE INFORMATIQUE IMMÉDIATE :** En cas de problème informatique, il suffit de contacter le Centre de service du prestataire en infogérance par téléphone. 

- **MAINTENANCE PRÉVENTIVE :** La supervision informatique en entreprise a pour objectif d’effectuer une maintenance préventive grâce à un système de surveillance et monitoring. Cela permet d’éviter les incidents et de prévenir l’arrêt ou la baisse d’activité dus à l’irruption de problèmes informatiques (pannes, intrusions, virus…)

- **ADAPTER LE SYSTÈME D’INFORMATION AUX BESOINS DE L’ENTREPRISE :** assurer la cohérence de l'infrastructure aux besoins, faire évoluer l'infrastructure pour répondre aux besoins organisationnels...

- **UN INTERLOCUTEUR UNIQUE :** Inutile de se poser la question qui contacter. C’est votre infogérant qui fait office de guichet unique auprès de vos différents prestataires liés à l’informatique.

- **UN NIVEAU DE SERVICE ADAPTÉ AUX BESOINS :** Le principal avantage de l'infogérance est la liberté de choisir un niveau de service parfaitement adapté aux besoins réels de votre entreprsise en matière d'accompagnement informatique

- **MAÎTRISE DES COÛTS :** L’infogérance apporte cette maîtrise budgétaire que d’autres solutions d’accompagnement informatique n’ont pas.

DANS CE CAS, L’INFOGÉRANT A 2 MISSIONS :
1. Son travail au quotidien pour maintenir le système d’information fonctionnel
2. Sa mission consiste également à assurer la gestion du parc informatique. 

## 3 – Le cloud
**Recherchez les différences qui existent entre une offre cloud de type IAAS, PAAS et SAAS**

**IAAS ("Infrastructure As A Service") :** C'est une solution logicielle applicative hébergée dans le cloud et exploitée en dehors de l'organisation ou de l'entreprise par un tiers, aussi appelé fournisseur de service.

**PAAS ("Platform As A Service") :** La solution PaaS vous permet d'économiser beaucoup de temps. Contrairement à la solution IaaS (Infrastructure as a Service), les couches de stockage, de serveurs, de virtualisation, de système d'exploitation, de middleware et de données qui sont gérés par votre fournisseur, sont disponibles immédiatement.

**SAAS : ("Software As A Service") :** C'est une solution logicielle applicative hébergée dans le cloud et exploitée en dehors de l'organisation ou de l'entreprise par un tiers, aussi appelé fournisseur de service.
Elle n'a donc pas besoin d'acquérir directement ces applications ni d'investir dans des serveurs pour les héberger.

**Pour chacun de ces types de cloud, donnez un exemple de votre connaissance.**

*Exemple de Iaas :*
- Microsoft Azure
- IBM Cloud

*Exemple de Paas :* 
- Héroku
- Dokku

*Exemple de Saas :*
- Wrike
- Freshsales

**Vous allez devoir estimer coût d'une solution de Cloud**

**Coût mensuel dans la zone Région France (Iaas):**

- Ordinateur virtuel serveur Windows standard en tant qu'instance réservée sur 3 année avec la
licence Azure Hybrid Benefit : 30.71€ par mois 

- D'un ordinateur virtuel serveur Linux Ubuntu standard en tant qu'instance réservée sur 3 année :

**Coût mensuel dans la zone Région France (Paas):**

- D'un serveur SQL Server en tant qu'instance réservée sur 3 année avec la licence Azure Hybrid
Benefit : 

**Coût mensuel dans la zone Région France (Saas):**

- D'un serveur de données multimédia (Média Services) de diffusion de vidéo en direct : 


# 4 – Le patrimoine informatique

**Définir la notion de patrimoine informatique :** Le patrimoine informatique est constitué de matériaux fondés sur l'informatique, d'une valeur durable, qu'il est nécessaire de conserver pour les générations futures. Il émane de communautés, de régions, d'industries et de secteurs différents.

**Définir la notion de parc informatique :** L'expression « parc informatique » désigne l'ensemble du matériel informatique et des logiciels utilisés au sein d'une entreprise

**Ce qui distingue le patrimoine informatique du parc informatique :** 
Le terme de parc informatique désigne l'ensemble des ressources matérielles et logicielles qui composent votre système informatique. Le parc informatique comprend : Les postes de travail fixes ou portables,unités centrales et leurs accessoires : écran, claviers, souris…etc.
