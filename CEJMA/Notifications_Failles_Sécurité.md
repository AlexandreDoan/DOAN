## Notifications des Failles de Sécurité
## Doan Alexandre
## 16/03/2021 

### I - Citez les différents types de faille de Sécurité

Les différents types de faille de sécurité sont les suivantes : 

- La gestion et diffusion des accès aux utilisateurs 
- Les logiciels et applications obsolètes
- L'Hébergement de vos données 
- L'Utilisation des équipements personnels
- La Messagerie électronique.

### II - Quels sont les différents type de processus de notification prévu par le RGPD (différentes notifications ; Quel délai ; À qui ; les sanctions de la CNIL ; Montant maximale de l'amende).

Il y a un rapport à rédiger et remettre à la CNIL, ce rapport doit contenir la nature de la faille, quel public en est touché, le nombre de personnes concernées suivi des conséquences de la violation de sécurité ainsi que les mesures prévues.

Les entreprises ont un délais de 72H pour prévenir d'une violation de données personnelles. Il est possible de dépasser ce délais mais il faudra le justifier auprès de la CNIL.

La CNIL se chargera de l'étude de ce rapport et donnera des indications à l'entreprise comme informé les utilisateurs ou prendre des mesures techniques supplémentaires. Il est possible que la CNIL vous amende pour des mesures de sécurité pas suffisante. Le montant maximale d'une amende possible est de 20 millions d'euros ou 4% du chiffre d'affaires mondial.

### III - Politique interne de l'entreprise concernant les failles de sécurités notamment le cahier d'incident.

Afin d'indentifier ou déterminer les failles de sécurités, les entreprises enregistrent et tiennent un registre des actions effectué pour trouver la source de la faille.

Dans chaque entreprise un dispositif de gestion d'incident est mit en place pour assurer la reprise d'activité le plus rapidement possible et de réduir au maximum l'impact sur cette dernière. 

### IV - Citez 2 exemples (Entreprises concernées ; Date ; Faille ; Sanction de la CNIL ; Date de notification)

1er exemple : 

La société "Marriott" qui a été victime d'une faille de sécurité qui a entrainé une violation de données (Données personnelles;Noms;Adresses;Coordonnées bancaires; etc). Les utilisateurs ont été mit au courant dans les 72H en Novembre 2020.

La société indique que 383 millions de compte ont été compris dans cette attaque.

La faille de sécurité était présente depuis l'année 2014 mais identifié en 2018 et c'est pour cette raison que la société a été amendée d'un montant de 18,4 millions de livres soit 20 millions d'euros. 

2eme exemple :

La société "Apple" qui a eu une faille le 9 Mars 2021, nommée 0-Day, cette faille comporte 6 failles de sécurité. Ces dernières permettant de lancer du code malveillant via les applications de messagerie sans avoir besoin d'intervention de l'utilisateur. La faille résidait dans le composant de rendu des images (ImageIO framework) présent dans les appareils équipé du système d'exploitation iOS, macOS, watchOS et tvOS.

Sources : Sites Internet 

- https://www.ximi.fr/recette-rgpd-services-personne/gestion-incidents-donnees-personnelles/

- https://www.cnil.fr/fr/securite-tracer-les-acces-et-gerer-les-incidents
