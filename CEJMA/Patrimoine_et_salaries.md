# Alexandre DOAN
### 23/03/2022

# Le patrimoine informatique et les salariés
## Première partie : Le nomadisme dans l'organisation

### **Questions :**

**`Donnez une définition du BYOD :`**

L'acronyme "BYOD" qui signifie "bring your own device" (apportez votre équipement personnel de communication) correspond à l'usage d'équipements informatiques personnels dans un contexte professionnel. Cela peut se traduire par exemple par l'utilisation par un salarié de son propre ordinateur pour se connecter au réseau de l'entreprise. 

**`Recensez sous forme de tableau les avantages et inconvénients du BYOD pour une organisation :`**

| Avantages | Inconvénients |
| ------ | ------ |
| Permettre à l'employeur de réduire ses coûts de fonctionnement | En utilisant son propre appareil, le salarié stocke des données qui sont personnelles à l'entreprise |
| Offre la possibilité d'avoir une gestion plus simple du travail |
| Le salarié peut travailler en dehors des heures d'ouverture | 
| Le salarié peut travailler en télétravail |
| Le BYOD engendre une qualité de travail plus importante | 

**`Qu'est ce que shadow IT ?`**

"Shadow IT" est utilisé pour désigner des systèmes d'information et de communication réalisés et mis en œuvre au sein d'organisations sans approbation de la direction des systèmes d'information

**`Quels sont les risques que fait courir le shadow IT pour une organisation ?`**

Avec la présence du cloud, les employés s'adonnent parfois à certains abus en se permettant de tout stocker. Le risque de sécurité dans ces conditions est très grand. Il peut en effet y avoir des fuites de données, l'utilisation non autorisée de certains documents pour frauder et la violation de certaines règles de conformité. On note par ailleurs un risque d'incidents de cybersécurité de plus en plus élevé.

Une autre source de risque provient du fait que le shadow IT et le BYOD laissent les DSI dans l'incapacité d'évaluer le véritable risque de cyberattaque auquel elles sont exposées. La plupart des cyberattaques sont aujourd'hui liées aux actions d'un actuel ou ancien salarié de l'entreprise. Dans le shadow IT, la menace, c'est l'employé.

**`Précisez ce qu'une organisation devrait faire pour gérer les BYOD, en tenant compte des recommandations de la CNIL et du RGPD :`**

**`Les équipements privés peuvent-t-ils être explicitement autorisés par une organisation ?`**

**`Quels sont les risques identifiés par la CNIL dans l'utilisation des BYOD et les mesures possibles pour les gérer ?`**

**`En vous référant à l'usage de votre équipement personnel pour vos études au lycée Valadon, quelles mesures ou critères vous semble de voir être inclus dans la charte informatique du lycée pour l'usage des BYOD des étudiants et enseignants (équipements acceptés, conditions d'utilisation, applications du lycée utilisables, ressources numériques accessibles) ?`**

















