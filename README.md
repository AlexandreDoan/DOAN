# Fiche présentation Alexandre DOAN 
## PARCOURS

|Année          | Etablissement| Niveau d'étude | Remarque |
|---------------|:-------------|:--------------:|--------------:|
|2019/2020|Lycée Simone de Beauvoir|Terminal STI2D|Obtention du Bac|
|2018/2019|Lycée Simone de Beauvoir|Terminal STI2D|Echec au Bac|
|2017/2018|Lycée Simone de Beauvoir|Premiere STI2D|///////////////////|


## TRAJET

_A pied_ **5 Rue Gallieni > Arret Carrières-Bel Air** |_5 minutes_|

_Bus 256_ **Arret Carrières-Bel Air > Gare de Deuil-Montmagny** |_15 minutes_|

_RER H_ **Gare de Deuil-Montmagny > Saint-Denis** |_10 minutes_| 

_RER D_ **Saint-Denis > Châtelet-Les Halles** |_20 minutes_|

_RER A_ **Châtelet-Les Halles > Neuville-Université** |_40 minutes_|

_Bus 34_ **Neuville RER > Le Noyer** |_15 minutes_|

_A pied_ **Le Noyer > Lycée de l'Hautil** |_5minutes_|

**_TOTAL~ 2 heure_**

## CHOIX BTS SIO

L'informatique m'intersse fortement, j'adore l'informatique, J'adore travailler sur mon propre ordinateur.

## POURQUOI CETTE ETABLISSEMENT 

Car j'ai été affecté par ParcourSUP par hasard a la derniere minutes.

## SISR OU SLAM 

J'aimerais allez en option SISR.

## POURQUOI

Car j'ai déjà fait presque 4 ans de programmation (ARDUINO) et j'ai envie de changé car j'aime les adresse IP et les bases de données je trouve sa très interresant.
Et j'aimerais devenir plustard Administrateur de bases de données j'aimerais apprendre davantage sur les bases de données.

## CE QUE VOUS CONNAISSEZ DEJA 

J'ai déjà "coder" je connais l'HTML, l'ARDUINO.
J'ai déjà utilisé des logiciel pour simuler mes capteurs comme Fritzing, Proteus, Filius. 

## FORCE ET FAIBLESSE 
| FORCE | FAIBLESSE |
|---------|---------|
|Serieux|Comprend lentement|
|Calme|Peut-être trop calme|
|Sympas|Travail peu a la maison à cause du trajet|
|Autonome||
|Je donne le meilleur de moi même pour ne pas être le dernier||

## CONCLUSION

JE SUIS UN ADULTE.
