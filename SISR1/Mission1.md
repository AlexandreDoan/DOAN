Doan Alexandre
Date de création : 14/12/2020

**_Etape 1_**


**Quels sont les sous-réseaux logiques représentés dans ce réseau physique ? Justifier.**

Il y a deux sous-réseaux logiques dans ce réseau physique : 

M1, M2, M3 : 192.168.1.0/24 

M4, M5, M6 : 172.12.0.0/16

**Compléter la configuration du routeur pour permettre à toutes les machines de communiquer entre-elles ? Spécifier toutes les étapes nécessaires.**

-Prendre l'adresse de passerelle de chaque sous réseau et les mettre en adresse IP dans le routeur.

-Configurer le routeur et enfin, modifié le masque pour qu'il correspond à celui de chaque sous réseau.

**Quelle commande est utilisée pour permettre de tester la bonne communication entre les deux machines ?**

La commande pour tester la bonne communication entre les deux machines est `ping`.

**_Etape 2_**

**Cliquer sur chaque commutateur (Switch R1 & Switch R2) pour faire apparaître leur table de correspondance. Quelles informations y trouve-t-on ?**

L'information qu'on a est qu'il y a un tableau de 2 colonnes, un avec l'adresse MAC et un avec le port sur lesquels ils sont branchés.

**On choisit de faire communiquer M1 et M2. Mais avant, regarder la table des échanges de données et on la supprimera avec le menu contextuel "supprimer la table". Opérer la commande arp dans la console de chaque machine. Que voit-on ?**

Il y a un tableau contenant adresse IP et une adresse mac qui est 255.255.255.255 FF:FF:FF:FF:FF:FF

**Opérer un ping à partir de M1 pour tenter de joindre M2.
Quel chemin lumineux prend le message ? Voit-on des modifications dans la table de correspondance des Switch R1 & R2 ? Si Oui, lesquelles ? Afficher les échanges sur les machines M1 & M2 ? Quels types (protocole et couche) de messages ont été échangés ? Opérer encore la commande arp dans la console de chaque machine. Que voit-on ? Que remarque-t-on si on échange un nouveau ping entre M1 et M2 ?**


Tout d'abord la lumière va de M1 jusqu'au switch ensuite il va sur le routeur et au 3 machines (M1 M2 et M3) puis revient au switch et jusqu'à M2, la lumière passe par le switch et sont directement redirigé vers la bonne machine.
On constate que dans le switch R1 il y a deux machines qui sont apparu sur le port 0 et le port 1.
Sur la table des échanges, on voit qu'il y a eu un protocole ARP et ICMP, tous sur la couche "Internet". Lorsqu'on fait `arp` sur chaque machine il y a une nouvelle adresse IP enregistrée.

**Recommencer les étapes 1 à 3, en choisissant de faire communiquer M3 et M6. Quels nouveaux changements voit-on ?**

Tout d'abord la lumière va de M3 jusqu'au switch après elle va vers M1,M2 et le routeur, ensuite il revient au switch puis repart sur M3 enfin elle va jusqu'au routeur, et repart jusqu'au switch.

**_Etape 3_**

**A quoi sert le serveur DNS dans un réseau physique ?**

Le serveur DNS sert à traduire un nom de domaine en adresse IP pour qu'une machine puisse accéder à une autre depuis un réseau.

**Les machines M1 et M6 sont utiles à tous les utilisateurs du réseau physique et on aimerait leur permettre d'y accéder de manière plus aisée. Comment le serveur DNS y contribuera ?**

Le DNS va permettre aux machine de lire les adresses ip sous forme de nom de domaine en enregistrant l'adresse du serveur DNS sur les machines M1 et M6 en plus de chaque machine qui souhaitent communiquer avec plus facilement.

**Ajouter une machine de type tour qu'on appellera 'Serveur DNS'. On la connectera à la troisième carte réseau du routeur. Quelle adresse logique lui donner ? Compléter la configuration de cette nouvelle machine pour lui permettre de communiquer avec toutes les machines du réseau physique et vice-versa. Installer un serveur DNS via l'utilitaire d'installation logiciel de Filius. Configurer le serveur pour donner un nom particulier à M1 et M6. Noter les étapes. Démarrer le serveur.
Tout d'abord on lui donne une adresse logique qui ne correspond pas à un sous reseau déjà utilisé puis on lui donne une adresse de passerelle pour le routeur et une adresse de Serveur DNS. Ensuite on installe un serveur DNS dessus. Le nom de domaine sera "M1" ou "M6" et l'adresse IP sera "192.168.1.145" ou "172.12.255.254

**Est-il possible d'accéder à M1 et M6 via leur nouveau nom ? Sinon, quelles configurations supplémentaires sont nécessaires pour y parvenir ?**

Oui on peux accéder car l'adresse IP du serveur DNS est déjà entré dans chacune des machines souhaitant communiquer avec M1 ou M6 (ainsi que dans M1 et M6)

**Analyser les trames/paquets échangés lors de la mise en communication de M5 et M1 par exemple. Voit-on des nouveaux types de paquets ? Et entre quelles machines sont-ils échangés ?**

Il y a un nouveau type de paquet échanger lors de l'essai de communication entre M5 et M1. Ils apparaissent en bleu, n'ont pas de protocole mais une couche Application.


