### DOAN Alexandre
### BTS 1 
### 04/02/21
# Mission4 : Installation d'un serveur SSH

## Etape 1 - Préparation d'une nouvelle machine

3. Pour mettre à jour la nouvelle VM la commande est `apt update` permettant de mettre à jour vos programmes/paquets installés dans votre système en se basant sur les sources définies dans `/etc/apt/source.list`.
`APT upgrade` installe les mises à jour identifiée avec la commande "apt update" sans supprimer les paquets installés.

## Etape 2 - Un serveur SSH ?

1) SSH : - Permet de se connecter de n'importe ou à sa machine en ligne de commande.
L'utilisation de ssh permet de chiffré les données qui transitent via le réseau.

2) Les moyens d'authentification possibles pour accéder à un serveur SSH sont pat le mot de passe et par la clé publique et privée du client.

3) Le port par défaut d'un serveur SSH est le port 22.

## Etape 3 - Installation du serveur SSH

1) Je regarde si le paquet "ssh" est installé via la commande `apt-cache policy openssh-server`.
![ssh](Capture M4/ssh.PNG)  
On peut voir que le paquet "ssh" n'est pas installé.

Nous allons donc lancer l'installation SSH avec la commande: `sudo apt install openssh-server`.
![ssh](Capture M4/Capture1.PNG)  


Le chemin du fichier de configuration est : home/user/etc/ssh/ssh_config



## Etape 4 - Test du fonctionnement du serveur SSH
## Etape 5 - Gestion de paires de clés privée et publique
