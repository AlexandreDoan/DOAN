### Doan Alexandre
### BTS 2 / BLOC 1
### Date de création : 06/10/2021
### Date de modification : 06/10/2021
# Changement d'horaires via sio-hautil
#### Problème 1 : Nous devons effectuer un changement d'horaires sur notre machine sur le serveur sio-hautil

Voici les commandes qui permet de réaliser un changement d'horaire :

`date` : Affiche la date et l'heure du jour.

`tzselect` : Choisir un continent + le pays concerné.

![config 1](SISR2/img/tzselect.PNG)

Ensuite on change dans le fichier de configuraton on ajoute "TZ='Europe/Paris'; export TZ"

![config 1](SISR2/img/Capture555.PNG)

`nano .profile` : TZ='Europe/Paris'; export TZ

![config 1](SISR2/img/22.PNG)
 
Enfin on se deconnecte du serveur ssh et on se reconnecte et l'heure s'affiche correctement.

#### Problème 2 : Nous devons créer un alias sur le serveur sio-hautil


`mc` : midnight commander

![config 1](SISR2/img/Capture7.PNG)

`nano .bashrc` : Modifier le fichier de configuration 

![config 1](SISR2/img/alias.PNG)

On a ajouter un alias ll qui nous évite de taper la commande ls -rtlh. Du coup on se deconnecte du serveur ssh et on se reconnecte et l'alias s'affiche correctement.
