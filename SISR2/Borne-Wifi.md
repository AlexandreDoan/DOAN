### Doan Alexandre
### BTS 2
### Date : 08/10/2021


# Borne wifi installation et configuration + switch POE Aruba 

- Installation des équipements et schéma réseau 

![](img/DHCP/apschéma_1_.png)

- Configuration des équipements

Nous avons utilisé un scanner réseau ip qui nous a permis d'identifier chaque machine dans le réseau et nous avons trouver les adresses Ips et les adresses mac du switch et de la borne Wifi. 

- Switch POE
La Configuration à était éffectué dans le CLI de l'appareil avec une connexion en SSH à l'aide de l'outil putty. 
La 2éme méthode de connexion est en filaire en série avec le port console du switch POE on a modifié son adresse Ip et mis en statique.

Grace à cette connection j'ai réussi à modifier les différents vlans pour configurer le réseau.

- Borne Wifi

**Login:** admin

**password**: ADMIN

**Nom:** C-E63-5

**Adresse réseau:** 10.0.31.157

**Passerelle:** 10.0.0.1

**Adresse Mac:** E82689C6E2B2

**Masque de sous réseau:** 255.0.0.0

La configuration à été éfféctué via l'interface graphique et en CLI on a modifié l'adresse Ip dynamique en static et renomé le nom de la borne Wifi.

![](SISR2/img/CLI.png)

- Configuration Graphique:

Pour avoir un accés graphique à cette configuration j'ai modifié les configurations de proxy et réinitialiser (reset) la borne Aruba car les identifants par défaut ne fonctionner pas.

Après l'avoir réinitialiser j'ai utilisé : `admin` pour le login et (le SN de la borne) pour mot de passe par défaut.

**Point D'accés :** 

![](img/point-d_accés.png)

**Utilitaire :**

![](SISR2/img/Utilisationap.png)

**Affectation :**

![](SISR2/img/affectation-ip.png)

**Sécurité :**

![](img/sécurité.png)

**Accès :**

![](img/Accés.png)

Connection à la borne : 

**LOGIN :** SISR  ou SLAM

**PASSWORD :** BTS_SISR ou BTS_SLAM 

*Problème au cas où :*

*- Vérifier si on es bien dans le réseau BTS SISR*

*- L'adresse réseau à changer car on avait reset (10.0.31.157 -> 10.0.31.186)*

*- Si il y a un problème pour accéder à la borne Wifi, désactiver le proxy*

*- Ma borne principal c'est le SLAM*
