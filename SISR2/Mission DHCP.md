## Mission DHCP
## Alexandre Doan
#### BTS 2 - 08/11/2022

## Etape 1 - Préparer un réseau-test
(j'ai fait une erreur j'ai mis trop de mémoire vive c'est-à-dire 4000Mo dans mes 4 machines virtuel alors que ma machine n'a que 8Go de RAM)

Voici la machine Linux et les 3 autres machines clients 

![config 1](SISR2/img/DHCP/Capture1.PNG)

## Etape 2 - Installer etckeeper 

La commande qui permet d'installer etckeeper est `sudo apt-get install etckeeper`.

![config 1](SISR2/img/DHCP/Capture2.PNG)
![config 1](SISR2/img/DHCP/Capture3.PNG)

## Etape 3 - Installer le serveur DHCP et Tshark 

La commande pour installé le serveur DHCP est `sudo apt install isc-dhcp-server`.

![config 1](SISR2/img/DHCP/Capture4.PNG)

Le fichier de configuration du serveur DHCP est sous `/etc/dhcp` et il se nomme `dhcpd.conf`.

![config 1](SISR2/img/DHCP/Capture5.PNG)

La commande pour démarrer le serveur DHCP est `systemctl start isc-dhcp-server`.

La commande pour arrêter le serveur DHCP est `systemctl stop isc-dhcp-server `.

La commande pour redémarrer le serveur DHCP est `systemctl restart isc-dhcp-server `.

Quand j'effectue la commande pour démarrer le serveur DHCP il y a une erreur : 

![config 1](SISR2/img/DHCP/Capture_6.PNG)

![config 1](SISR2/img/DHCP/Capture7.PNG)

Pour accéder au ficher config je dois faire `sudo nano /etc/network/interfaces`.

![config 1](SISR2/img/DHCP/Capture8.PNG)

J'ai changer "dhcp" en remplaçant par static.

![config 1](SISR2/img/DHCP/Capture12.PNG)

Je modifie le fichier "dhcpd.conf".

![config 1](SISR2/img/DHCP/Capture10.PNG)

Je modifie aussi le fichier "isc-dhcp-server" qui est dans "etc/default/"

![config 1](SISR2/img/DHCP/Capture13.PNG)

![config 1](SISR2/img/DHCP/Capture14.PNG)

Après avoir modifier tout les fichiers config le dhcp marche.

![config 1](SISR2/img/DHCP/Capture15.PNG)

TShark, est un analyseur de protocole de réseau comme Wireshark mais il vous permet de capturer des données de paquets d’un réseau en direct, ou lire des paquets à partir d’un fichier de capture précédemment enregistré, permet d'écrire les paquets dans un fichier. Format de fichier de capture de TShark est pcap format, ce qui est également le format utilisé par tcpdump et divers autres instruments. Cette installation est utile car Tshark est capable de capturer les informations des paquets de données de différentes couches de réseau et de les afficher dans différents formats.

Pour installé Tshark on fait `sudo apt-get install tshark`.

(Pour installé Tshark sur les machines clientes je devais régler la connexion car je n'avais pas de réseau je test le ping pour voir si j'ai bien accès à Google avec `ping 8.8.8.8` ensuite j'ai fait `sudo apt-get update` et `sudo apt-get upgrade` mais cela n'a pas marcher j'ai du faire `sudo apt update` et ensuite `sudo apt upgrade` ensuite après avoir fait les mise à jours je peux enfin installé Tshark).

Commande pour capturer le traffic Tshark: `sudo Tshark -i enp0s3`
liste les interface `Sudo Tshark -D ` 

capture le réseau et enregistre une copie de la capture .pcap:

`tshark -i enp0s3 -w <file-name>.pcap`

Lire une capture déja enregisté .pcap:

`tshark -r file-name.pcap`

capturer et voir le traffic en direct:

`tshark -i enp0s3 -f`

## Etape 4 - Configurer le réseau de machines 

Il faut isoler les machines virtuelles du réseau extérieur car le serveur DHCP ne pourra pas fournir d’adresse ip aux machines car il existe d’autre serveur DHCP et il est possible qu’un autre serveur DHCP du réseau extérieur fourni une adresse ip à la place du serveur DHCP qu’on utilise donc les machines n’appartiendront pas au même réseau.

Pour isoler les machines il faut modifier les paramètres du serveur DHCP ainsi que chaque machine de son réseau et les mettres dans le même réseau interne :

![config 1](SISR2/img/DHCP/Reseau_intern.PNG)

Pour que chaque machine obtienent une adresse ip automatique il faut ajouter dans le fichier de configuration interface et réseau de chaque machines CLIENTS on ajoute `dhcp`.

![config 1](SISR2/img/DHCP/dhcp_client.PNG)

On utilise la commande `dhclient` elle permet de fournir une configuration ip automatique grâce au serveur DHCP.

Pour voir les adresses ip automatiques qu'on a récupérés on fait `ip r`ou `ip a`.

Les adresses ip automatiques récupérés sont :

**- Ip dhcp :** 192.168.1.6/24

**- Client 1 :** 192.168.1.3/24

**- Client 2 :** 192.168.1.5/24

**- Client 3 :** 192.138.1.4/24

La commande qui permet de redemander une nouvelle ip sur Windows est `ipconfig/release` et ensuite `ipconfig/renew`.

La commande qui permet de redemander une adresse ip sur linux est `dhclient`.

La commande qui permet de renouveler le bail est `nano /etc/dhcp/dhcpd.conf`

On ouvre le fichier de configurer `nano /etc/dhcp/dhcpd.conf`.
Biensûr toujours quand on utilise une commande on met `sudo` pour utilisé en superutilisateurs sinon on a pas les droits.

![config 1](SISR2/img/DHCP/delay.PNG)

On peut donc changer la valeur par défaut et augmmenter ou diminuer le bail.

## Etape 5 - Test du fonctionnement DHCP et capture de trames avec Tshark  

Tout d'abord on utilise la commande `tshark -D` pour visualiser tout les réseaux, on sélectionne le réseau enp0s3. Ensuite pour capturer les packets, on utilise une machine cliente pour demander une nouvelle IP à la machine serveur (DHCP). Et ensuite on lance une capture avec la commande `sudo tshark -i enp0s3 -w alex_capture.pcap`.

Quand on a finis la capture on enregistre mais pour l'enregistrer il faut créer un répertoire dans notre machine virtuel avec la commande `mkdir`.

![config 1](SISR2/img/pwd.PNG)

A partir d'ici je n'arrive pas à récupérer sur ma machine hôte le fichier pcap pourtant j'ai réussit à envoyé le ficher pcap sur sio-hautil.eu. J'ai utilisé la commande `scp` mais aussi `rsync` et je n'ai pas réussit 

![config 1](SISR2/img/tshark.PNG)

![config 1](SISR2/img/Transfert.PNG)

J'ai réussit à trouver un moyen de transferer mon fichier pcap finalement avec la commande `scp doana@sio-hautil.eu:/home/p02/doana/public_html/alex_capture.pcap C:\Users\Farid\`

![config 1](SISR2/img/DHCP/commande_transfert.PNG)

![config 1](SISR2/img/DHCP/wireshark.PNG)

## Etape 6 - Configurations particulières  

- L'information primordiale pour identifier la machine est sa mac Addresse. On doit identifier sa mac Addresse pour pouvoir reconnaitre la machine car son adresse ip risque de changer à plusieurs reprise et l'adresse MaC est la seule qui ne change pas et permet d'identifier la machine et son fabriquant.

- Il faut affecter une adresse ip static qui appartient au même sous réseau que les autres machines clientes et surtout elles ne doivent pas etre attribuer par une autre machine.

- La configuration nécessaire est  : /etc/network/interfaces et on efface l’option DHCP on le remplace par static suivi d’une adresse qu’on souhaite lui donner.

- Pour pouvoir bannir une machine il faut créer une réservation grâce a l'adresse mac des adresses ip en dehors des plages utilisé.

- Les fichiers de configuration à paramétrer DHCP sont :

    `sudo /etc/dhcp/dhcpd.conf`

    `sudo /etc/default/isc-dhcp-server`
    
    `sudo /etc/network/interface`
