## Mission DNS
## Alexandre Doan
#### BTS 2 - 08/11/2022

## Etape 0 - Préparer un réseau-test
(J'utilise les machines de l'ancien mission qui est le DHCP)

Voici les 4 machines :
- Une machine vierge pour accueillir le serveur DNS
- Une deuxième machine existante avec le serveur DHCP
- Et deux machines vierges

![config 1](SISR2/img/DNS/etape0.PNG)

J'ai installer etckeeper avec la commande `sudo apt-get install etckeeper`.

![config 1](SISR2/img/DNS/etckeeper.PNG)

## Etape 1 - Schéma réseau à jour de la situation entière  
## Etape 2 - Installer le serveur DNS et Tshark
Le serveur DNS est installer.

![config 1](SISR2/img/DNS/dns.PNG)


- La commande pour installé le serveur DNS est `apt install bind9`
 
- Le fichier de configuration est `/etc/bind`

- La commande qui permet de démarrer le service DNS est `systemctl start bind9 `

- Pour arrêté `systemctl stop bind9`

- Pour redémarrer `systemctl restart bind9`

J'ai installer tshark sur toute les machines grâce a la commande `sudo apt install tshark`.

![config 1](SISR2/img/DNS/tsharkk.PNG)

## Etape 3 - Configurer le réseau de machines 

Pour configurer le réseau pour que chaques machines appartiennent au même réseau et ne communique pas avec d'autres réseaux extérieurs, on configure le mode d'accès réseau en réseau interne.

![config 1](SISR2/img/DNS/interne.PNG)


L'isolement des machines virtuelles avec l'extérieur permet au serveur DHCP d'être le seul a fournir une configuration IP aux machines de sont sous réseau et empêche les collisions avec un autre DHCP.

La configuration pour récupérer une adresse automatique on utilse la commande `sudo nano /etc/network/interfaces`. 
Pour que chaque machine obtient une adresse ip automatique il faut ajouter dans le fichier de configuration interface et réseau de chaque machine client on ajoute `dhcp` dans la configuration

![config 1](SISR2/img/DNS/config.PNG)
![config 1](SISR2/img/DNS/configALL.PNG)
Les adresses ips fourni par le DHCP sont `10.0.2.15/24 et 10.0.2.15/24 `.

Normalement j'obtient different adresse IP mais ce n'est pas grave il n'y a rien de mal à ce que toutes nos machines obtiennent la même adresse dans la configuration NAT car toutes les machines virtuelles sont isolées les unes des autres, il n'y a donc aucun risque de conflit. Je pense que si la configuration aurais été en accès par pont sa n'aurais pas marcher.

![config 1](SISR2/img/DNS/adresseIP.PNG)

**Configuration du DNS :** 

L'adresse Ip de la machine est 192.168.1.10 

![config 1](SISR2/img/DNS/8888.PNG)

On modifie le fichier de configuration `sudo nano /etc/bind/named.conf.local` il permet d'organiser les différents dns créer.

![config 1](SISR2/img/DNS/5.PNG)

On modifie 

![config 1](SISR2/img/DNS/6.PNG)

Ensuite on modifie le fichier de configuration `sudo nano /etc/bind/db.local` il permet de configurer et faire la résolution d'addresse avec le nom de domaine choisi ainsi que de choisir la date d'expiration et le cache du DNS.

![config 1](SISR2/img/DNS/7.PNG)

![config 1](SISR2/img/DNS/8.PNG)

Maintenant on lance le service bind9 avec la commande `systemctl start bind9` et ensuite on verifie si il est bien allumé `systemctl status bind9`.

![config 1](SISR2/img/DNS/9.PNG)

Après avoir configurer le serveur DNS on le test avec la commande `dig` , on doit d'abord l'installer avec `sudo apt install dnsutils`.

![config 1](SISR2/img/DNS/install.PNG)

Ensuite on test la commande dig sur une machine cliente `dig alexbind.fr `.

![config 1](SISR2/img/DNS/10.PNG)

On peut voir qu'on obtient une réponse ci-dessus Le serveur DNS est fonctionnel .

## Etape 4 - Test du fonctionnement DNS et capture de trames avec Tshark  

- Le filtre qui permet de voir uniquement le paquet dns est `-f`  Commande complète: `tshark -i enp0s3 -f dns`

- Pour vérifier le fonctionnement du Server DNS ont utilise la commande`nslookup` pour vérifier si la résolution d'adresse a bien été éfféctué dans les fichiers de configuration, et nous utilisons la commande `dig` cette commande permet de contacté le serveur dns 

![config 1](SISR2/img/DNS/15.PNG)

- Je créer un dossier dans ma machines clientes `mkdir alex ` et je me donne les droits d'utilisateur `sudo chmod 777 alex ` et ensuite je fait la capture `sudo tshark -i enp0s3 -w dns_capture.pcap `. 

- Pour transférer dans mon pc j'utilise le protocole SSH je transfere d'abord sur le serveur SSH depuis la machine cliente a l'aide de cette commande `sudo scp dns_capture.pcap doana@sio-hautil.eu:/home/p02/doana/public_html `

![config 1](SISR2/img/DNS/333.PNG)

- Ensuite depuis le serveur SSH je transfère dans ma machine, j'ouvre le terminale et j'effectue la commande: `rsync`.
![config 1](SISR2/img/DNS/rsync.PNG)

Malheureusement la commande rsync est mieux sur debian et ma machine hote est un Windows du coup j'ai utilisé cette commande `scp doana@sio-hautil.eu:/home/p02/doana/public_html/dns_capture.pcap C:\Users\Farid\`.

![config 1](SISR2/img/DNS/444.PNG)

- Lorsque j'utilise la commande `dig alexbind.fr` sur une machine cliente voici le résultat.

![config 1](SISR2/img/DNS/wire.PNG)



