### Doan Alexandre
### BTS 2
### Date : 12/10/2021

# Certifications déjà passé :

**MOOC ANSSI** : https://secnumacademie.gouv.fr/

**MOOC RGPD** : (Pas encore passé je dois demander à Mme Mouly comment le repassé) https://www.cnil.fr/fr/le-mooc-latelier-rgpd-fait-peau-neuve

**PIX** : Examen à la fin de l'année 

# Certifications à passer :

**Introduction to IoT** : Découvrez comment l'Internet des objets révolutionne le monde et quelles sont les compétences nécessaires pour décrocher un emploi bien rémunéré. 

- Apprendre comment la transformation numérique crée aujourd'hui des opportunités économiques inédites.
- Découvrir comment l'IoT rapproche les systèmes IT et les technologies opérationnelles.
- Découvrez comment les processus commerciaux standard se transforment.
- Découvrez les problématiques de sécurité à prendre en compte lors de l'implémentation de solutions IoT.

Niveau : Débutant

**NDG Linux Unhatched Français 1021 cga** : Un cours qui s'attaque aux bases de Linux.

- Réaliser une installation et une configuration de base des logiciels Linux.
- Connaître les bases de l'interface de ligne de commande Linux.
- Interagir avec la machine virtuelle Linux.
- Déterminer si Linux est fait pour vous ou pas.

Niveau: Débutant

**Introduction to Cybersecurity Français 1021 cga** : Le cours d'introduction s'adresse à ceux qui souhaitent découvrir le monde de la cybersécurité.

- En savoir plus sur la cybersécurité et son impact potentiel sur vous.
- Connaître les menaces, les attaques et les vulnérabilités les plus courantes.
- Découvrir comment les entreprises protègent leurs activités contre les attaques.
- Découvrir les dernières tendances en matière d'emploi et pourquoi la cybersécurité gagne du terrain.

Niveau: Débutant

**Cybersecurity Essentials** : Pour apprendre et assimiler les bases et les pré-requis avant d'entamer une formation en cybersécurité.
- Connaître les contrôles de sécurité appliqués aux réseaux, aux serveurs et aux applications.
- Apprendre des principes de sécurité utiles et découvrir comment élaborer des politiques conformes.
- Mettre en œuvre des procédures appropriées en matière de confidentialité des données et de disponibilité.
- Développer un esprit critique et des compétences en matière de résolution des problèmes en utilisant des équipements réels et Cisco Packet Tracer.

Niveau : Intermédiaire

**Networking Essentials** : Pour apprendre et assimiler les bases et les pré-requis du réseau.

- Planifiez et installez un réseau à domicile ou dans une PME à l'aide de technologies sans fil, et connectez-le à Internet.
- Développez un esprit critique et des compétences en matière de résolution des problèmes en utilisant Cisco Packet Tracer.
- Mettez en pratique la vérification et le dépannage du réseau et de la connectivité Internet.
- Identifiez et prévenez les menaces visant un réseau à domicile.

Niveau : Intermédiaire

**Cloud Security** : Apprendre à concevoir, à créer et gérer un environnement professionnel cloud sécurisé.

- Développer une connaissance approfondie des fonctionnalités complètes du cloud computing.
- Comment développer efficacement un programme global de sécurité du cloud conforme aux standards mondialement reconnus.
- Comprendre les bonnes pratiques pour la gestion des accès et des identités (IAM), la gestion des incidents dans le cloud, la sécurité des applications, le chiffrement des données, la sécurité en tant que service et la sécurisation des technologies émergentes.
- Préparer la certification CCSK (Cloud of Knowledge Security).

Niveau : Intermédiaire

