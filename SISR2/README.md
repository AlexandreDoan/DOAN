## Sommaire
## Alexandre Doan
#### BTS 2 - 04/01/2022


# Voici les travaux que j'ai effectué : 

- La borne Wifi "https://gitlab.com/AlexandreDoan/DOAN/-/blob/master/SISR2/Borne-Wifi.md"


- La mission DHCP "https://gitlab.com/AlexandreDoan/DOAN/-/blob/master/SISR2/Mission%20DHCP.md" + "https://gitlab.com/AlexandreDoan/DOAN/-/blob/master/SISR2/img/DHCP/alex_capture.pcap"


- La mission DNS "https://gitlab.com/AlexandreDoan/DOAN/-/blob/master/SISR2/Mission_DNS.md" + "https://gitlab.com/AlexandreDoan/DOAN/-/blob/master/SISR2/img/DNS/dns_capture3.pcap" 


- La mission AdminSysMariaDB "https://gitlab.com/AlexandreDoan/DOAN/-/blob/master/SISR2/CR_AdminSysMariaDB.md"


- Le DM fait en classe "https://gitlab.com/AlexandreDoan/DOAN/-/blob/master/SISR2/CR_DM1.md"
