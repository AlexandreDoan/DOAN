### Doan Alexandre
### BTS 2 / BLOC 1
### Date de création : 09/09/2021
### Date de modification : 09/09/2021
# Transfert de données d'un serveur SSH à un autre 
#### Problème 1 : le serveur `ssh doana@sio-hautil.eu` ne marche pas 

#### Solution 1 : nouveau serveur `ssh doana@vps327296.ovh.net`

Tout d'abord le serveur que j'utilise (ssh doana@sio-hautil.eu) ne marcher pas j'ai dû utilisé un autre serveur(ssh doana@vps327296.ovh.net) , la première connexion n'a pas marcher car la clé n'etais pas la bonne et du coup j'ai utilisé `pwd` pour trouver le chemin de la clé et j'ai supprimer la 1ère ligne.

Ensuite après avoir supprimer la clé j'ai réussit à rentrer dans le serveur puis avec le même mots de passe que le serveur sio-hautil.eu j'ai modifier le mots de passe avec `passwd`. J'ai retrouvé tout mes travaux de l'année dernière sur le serveur ovh.

Vu que le serveur ovh marche, le serveur sio-hautil.eu marche aussi, mais le serveur sio-hautil.eu est vide. 

#### Problème 2 : le serveur sio-hautil.eu est vide

#### Solution 2 : transférer tout les travaux du serveur ovh.net vers sio-hautil.eu 

J'ai trouver 2 commandes qui permet de transférer les données d'un serveur SSH à un autre.
Les 2 commandes sont `scp` et `rsync`, mais le proffeseur à dit la commande `scp` est une ancienne méthode il faut que l'on utilise la commande `rsync`.

La commande pour installé `rsync` est `sudo apt-get install rsync` mais on avais pas besoin de l'installé car il était déjà installé dans le serveur sio-hautil.eu.

La commande pour transférer les données est `rsync -avP doana@vps327296.ovh.net:/home/p02/doana/public_html .` 

![config 1](SISR2/img/3333333.PNG)

Enfin on utilise la commande `ls -rtlh` pour voir si public_html a bien été transférer :

![config 1](SISR2/img/789456.PNG)

Je revérifie bien que cela à bien transférer tout le public_html en rentrant dans son dossiers `ls -rtlh public_html/`
 
![config 1](SISR2/img/blabla.PNG)

Effectivement on peut voir que le transfére a marché .

Pour installer un serveur SSH : 

"tasksel"

"systemctl status sshd"

"adduser doan" 

psw:1561116516151651
